import React from 'react';

const button = (props) => (
    <div
        className="btn btn-dark"
        onClick={props.clicked}>
        {props.content} 
    </div>

);

export default button;