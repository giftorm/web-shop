import React, { Component } from 'react';
import './App.css';

import Header from './component/header/header';
import Wares from './component/wares/wares';
import Checkout from './component/checkout/checkout';
import Cart from './component/cart/cart';
import { Route, Switch } from 'react-router-dom';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Header />
        <Switch>
          <Route path="/" exact component={Wares}/>
          <Route path="/cart_summary" component={Cart}/>
          <Route path="/checkout" component={Checkout}/>
        </Switch>
      </div>
    );
  }
}

export default App;
