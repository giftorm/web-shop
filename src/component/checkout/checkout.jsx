import React from 'react';
import { connect } from 'react-redux';

import Button from '../../UI/button/button';
import { postOrder } from '../../requests';

class Checkout extends React.Component {
    state = {
        customerData: {
            email: "",
            first_name: "",
            last_name: "",
            address: "",
            country: "",
            zip: ""
        },
        ordered: false
    };

    
    submitOrder = () => {
        const customerData = this.state.customerData;
        const orderData = { products: this.props.itms };

        this.setState({ordered: true});
        
        postOrder(customerData, orderData);
        window.location.href = "/";
    }
    
    onChange = (event) => {
        let name = event.target.name;
        let customerData = this.state.customerData;
        customerData[name] = event.target.value;
        this.setState({customerData: customerData});
    }
    
    render() {
        let form = (
            <form>
                <div className="form-group">
                    <label>Email address</label>
                    <input 
                        type="email" 
                        name="email"
                        className="form-control" 
                        placeholder="Enter email"
                        value={this.state.customerData.email}
                        onChange={(event) => this.onChange(event)} />
                    <small className="form-text text-muted">We'll never share your email with anyone else.</small>
                </div>
                <div className="form-group">
                    <label>First Name</label>
                    <input
                        type="text"
                        name="first_name"
                        className="form-control"
                        placeholder="First Name"
                        value={this.state.customerData.first_name}
                        onChange={(event) => this.onChange(event)} />
                </div>
                <div className="form-group">
                    <label>Last Name</label>
                    <input
                    type="text"
                    name="last_name"
                    className="form-control"
                    placeholder="Last Name"
                    value={this.state.customerData.last_name}
                    onChange={(event) => this.onChange(event)} />
                </div>
                <div className="form-group">
                    <label>Address</label>
                    <input
                        type="text"
                        name="address"
                        className="form-control"
                        placeholder="Address"
                        value={this.state.customerData.address}
                        onChange={(event) => this.onChange(event)} />
                </div>
                <div className="form-group">
                    <label>Country</label>
                    <input
                        type="text"
                        name="country"
                        className="form-control" placeholder="Country"
                        value={this.state.customerData.country}
                        onChange={(event) => this.onChange(event)} />
                </div>
                <div className="form-group">
                    <label>Zip</label>
                    <input
                        type="text"
                        name="zip"
                        className="form-control"
                        placeholder="Zip"
                        value={this.state.customerData.zip}
                        onChange={(event) => this.onChange(event)} />
                </div>
                <Button
                    clicked={() => this.submitOrder()}
                    className="btn btn-primary"
                    content="Submit" />
            </form>
        );
        return (
            <div className="container">
                {this.state.ordered ? <p>THANKS FOR BUYING!<br /> Redirecting...</p> : form }
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        itms: state.shoppingbag.items,
    };
};

export default connect(mapStateToProps)(Checkout);