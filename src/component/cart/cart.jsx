import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as actionTypes from '../../store/actions';
import Button from '../../UI/button/button';
import { NavLink } from 'react-router-dom';

class Cart extends Component {
    render() {
        let shoppingCartItems = ( <p>hello</p> );
        if ( this.props.itms ) {
            shoppingCartItems = this.props.itms.map(item => 
                <tr key={item.id}>
                    <td>{item.name}</td>
                    <td>{item.price} $</td>
                    <td>{item.amount}</td>
                    <td onClick={() => this.props.onRemoveItem(item.id)}>-</td>
                    <td onClick={() => this.props.onAddItem(item.id, item.price)}>+</td>
                </tr>
            );
        }

        let shoppingCart = (
            <table className="table">
                <tbody>
                    {shoppingCartItems}
                </tbody>
            </table>
        );

        const checkoutButton = (
            <NavLink
                to="/checkout"
                exact>checkout
            </NavLink>
        );
        
        return (
            <div>
                {shoppingCart}
                <p>TotalPrice: {this.props.ttp}</p>
                <div className="co-btns">
                    <NavLink
                        className="btn btn-dark"
                        to="/"
                        exact>back
                    </NavLink>
                    <Button
                        disabled
                        className="btn btn-dark"
                        content={checkoutButton} />
                </div>
            </div>
        );
    }
};

const mapStateToProps = (state) => {
    return {
        ttp: state.shoppingbag.totalprice,
        itms: state.shoppingbag.items,
        disp : state.interface.displayCheckout
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        onToggleView: () => dispatch({ type: actionTypes.TOGGLE_VIEW }),
        onRemoveItem: (id) => dispatch({ type: actionTypes.REMOVE_ITEM, key: id }),
        onAddItem: (id, price) => dispatch({ type: actionTypes.ADD_ITEM, key: id, price: price }),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Cart);