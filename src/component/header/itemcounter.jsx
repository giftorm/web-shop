import React, { Component } from 'react';
import { connect } from 'react-redux';

class ItemCounter extends Component{
    render () {
        return (
            <li className="list-group-item">
                {this.props.ctr}
            </li>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        ctr: state.shoppingbag.counter 
    };
};

export default connect(mapStateToProps)(ItemCounter);