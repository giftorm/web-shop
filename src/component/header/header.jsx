import React from 'react';
import classes from './header.module.sass';

import ItemCounter from './itemcounter';

const header = () => (
    // <div className={classes.Header}>
    <div className="navbar navbar-expand-lg navbar-dark bg-dark">
        <p className={classes.HeaderText}><a href="/">web shop</a></p>
        <ul className="list-group">
            <ItemCounter />
        </ul>
    </div>
);

export default header;