import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as actionTypes from '../../store/actions';
import Item from './item/item';
import Button from '../../UI/button/button';
import { NavLink } from 'react-router-dom';

import { getWaresFromApi } from '../../requests';

class Wares extends Component {
    state = {
        itemList: [],
        loading: true,
    }
    
    componentDidMount() {
        const fetchedItems = getWaresFromApi();

        this.setState({
            itemList: fetchedItems
        })
    }

    render() {
        let items = null;
        if (this.state.itemList) {
            items = this.state.itemList.map( item => 
                <Item
                    key={item.id}
                    id={item.id}
                    name={item.name}
                    price={item.price}
                    clicked={this.props.onAddItem} />
            );
        }

        const continueToCart = (
            <NavLink
                to="/cart_summary"
                exact
                style={{display: 'block', height: '100%'}}
                >continue
            </NavLink>
        );

        return(
            <div className="container">
                {items}
                <Button disabled={!this.props.ctr} content={continueToCart} />
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        itms: state.shoppingbag.items,
        ctr: state.shoppingbag.counter,
        disp : state.interface.displayCheckout
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        onAddItem: (key, name, price) => dispatch({ type: actionTypes.ADD, itemData: { id: key, name: name, price: price } }),
        onToggleView: () => dispatch({ type: actionTypes.TOGGLE_VIEW }),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Wares);