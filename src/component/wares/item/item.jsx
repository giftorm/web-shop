import React from 'react';

import classes from './item.module.sass';

const item = (props) => {  
    return (
        <div className={classes.Item} onClick={() => props.clicked( props.id, props.name, props.price )}>
            <p>{props.name}</p>
            <p>price: {props.price}$</p>
        </div>
    );
}


export default item;