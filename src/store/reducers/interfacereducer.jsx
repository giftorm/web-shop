import * as actionTypes from '../actions'

const initialState = {
    displayCheckout: true
};

const reducer = (state = initialState, action) => {
    switch( action.type ) {
        case actionTypes.TOGGLE_VIEW:
            return { ...state, displayCheckout: ! state.displayCheckout }
        default:
    }
    return state;
};


export default reducer;