import * as actionTypes from '../actions'

const initialState = {
    items: [],
    counter: 0,
    totalprice: null
};

function getDuplicateItem (currentStateItemList, id) {
    for ( var i in currentStateItemList ) {
        if ( currentStateItemList[i].id === id ) {
            return 1;
        }
    }
    return 0;
}

const reducer = (state = initialState, action) => {
    switch( action.type ) {
        case actionTypes.ADD:
            let newStateItems = [ ...state.items ];
            let duplicateItem = getDuplicateItem(newStateItems, action.itemData.id);
            if ( duplicateItem ) {
                let updatedArray = [...state.items];
                for (let i in updatedArray ) {
                    if ( updatedArray[i].id === action.itemData.id ) {
                        updatedArray[i].amount = updatedArray[i].amount + 1;
                    }
                }
                return { ...state, items: updatedArray, counter: state.counter + 1, totalprice: state.totalprice + action.itemData.price };
            } else {
                const newItem = {
                    id: action.itemData.id,
                    name: action.itemData.name,
                    price: action.itemData.price,
                    amount: 1
                }
                return { ...state, items: state.items.concat(newItem), counter: state.counter + 1, totalprice: state.totalprice + action.itemData.price };
            }
            case actionTypes.REMOVE_ITEM:
                newStateItems = [ ...state.items ];
                let counter = state.counter;
                let totalprice = state.totalprice;
                newStateItems.map(item => {
                    if (item.id === action.key && item.amount > 0 ) {
                        item.amount = item.amount - 1;
                        counter = counter - 1;
                        totalprice = totalprice - item.price
                    }
                    return newStateItems;
                });
                return { ...state, items: newStateItems, counter: counter, totalprice: totalprice }
                case actionTypes.ADD_ITEM:
                counter = state.counter;
                totalprice = state.totalprice;
                newStateItems = [ ...state.items ];
                newStateItems.map(item => {
                    if (item.id === action.key) {
                        item.amount = item.amount + 1;
                        counter = counter + 1;
                        totalprice = totalprice + item.price;
                    }
                    return newStateItems;
                    })
                    return { ...state, items: newStateItems, counter: counter, totalprice: totalprice }
        default:
    }
    return state;
};


export default reducer;